gitlab-shell (12.2.0+debian-1) unstable; urgency=medium

  * New upstream version 12.2.0+debian

 -- Pirate Praveen <praveen@debian.org>  Wed, 22 Apr 2020 22:14:51 +0530

gitlab-shell (12.0.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Fri, 10 Apr 2020 19:17:08 +0530

gitlab-shell (12.0.0-1) experimental; urgency=medium

  * New upstream version 12.0.0

 -- Pirate Praveen <praveen@debian.org>  Mon, 06 Apr 2020 16:13:40 +0530

gitlab-shell (11.0.0+debian-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Tue, 31 Mar 2020 18:00:28 +0530

gitlab-shell (11.0.0+debian-1) experimental; urgency=medium

  * New upstream version 11.0.0+debian
  * Update README.source
  * Bump Standards-Version to 4.5.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 17 Mar 2020 14:10:16 +0530

gitlab-shell (10.3.0+debian-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop debian/tests/control that uses gem2deb-test-runner since this is not
    a Ruby package (Closes: #950230)

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 03 Feb 2020 12:34:07 +0100

gitlab-shell (10.3.0+debian-3) unstable; urgency=medium

  * Reupload to unstable (gitlab current version in unstable cannot be
    supported meaningfully)
  * Move section to net from ruby

 -- Pirate Praveen <praveen@debian.org>  Wed, 15 Jan 2020 21:42:34 +0530

gitlab-shell (10.3.0+debian-2) experimental; urgency=medium

  * Update minimum version of protobuf to 1.3
  * Add a maintscript to convert directory to symlink

 -- Pirate Praveen <praveen@debian.org>  Sat, 21 Dec 2019 00:10:54 +0530

gitlab-shell (10.3.0+debian-1) experimental; urgency=medium

  * New upstream version 10.3.0
  * Build only go files (ruby files are removed from this version)
  * Remove patches no longer required
  * Add README.source
  * Don't copy binaries to /usr/share
  * Hardcode gitlab-shell dir path

 -- Pirate Praveen <praveen@debian.org>  Fri, 13 Dec 2019 19:24:25 +0530

gitlab-shell (9.3.0+dfsg-1) experimental; urgency=medium

  * New upstream version 9.3.0+dfsg
  * Refresh d/patches
  * Fix package wrt cme
  * Ignore test failures
  * Bump debhelper-compat to 12
  * Bump Standards-Version to 4.4.1

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Mon, 21 Oct 2019 17:17:12 +0530

gitlab-shell (9.1.0+dfsg-2) experimental; urgency=medium

  * Remove comments in rules
  * Set gitlab shell directory with upstream patch (Closes: #929201)

 -- Pirate Praveen <praveen@debian.org>  Tue, 04 Jun 2019 22:06:08 +0530

gitlab-shell (9.1.0+dfsg-1) experimental; urgency=medium

  * New upstream version 9.1.0+dfsg

 -- Pirate Praveen <praveen@debian.org>  Wed, 15 May 2019 17:42:14 +0530

gitlab-shell (8.4.4+dfsg-1) unstable; urgency=medium

  * New upstream version 8.4.4+dfsg
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Fri, 15 Mar 2019 17:08:08 +0530

gitlab-shell (8.4.3+dfsg1-1) unstable; urgency=medium

  * Set minimum version golang-any to 1.10~
  * Use vendored copy of grpc and gitaly-proto

 -- Pirate Praveen <praveen@debian.org>  Sun, 16 Dec 2018 16:06:30 +0530

gitlab-shell (8.4.3+dfsg-3) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sat, 15 Dec 2018 15:50:37 +0530

gitlab-shell (8.4.3+dfsg-2) experimental; urgency=medium

  * Use more go binaries to replace old ruby binaries
  * Remove Breaks on gitlab as gitlab-shell binary is found (switched to go)

 -- Pirate Praveen <praveen@debian.org>  Sat, 15 Dec 2018 00:16:58 +0530

gitlab-shell (8.4.3+dfsg-1) experimental; urgency=medium

  * New upstream version 8.4.3+dfsg
    - Changed bin/gitlab-shell to bin/gitlab-shell-ruby
  * Add Breaks for gitlab << 11.5.3
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Dec 2018 19:55:50 +0530

gitlab-shell (8.3.3+dfsg-2) unstable; urgency=medium

  * Remove relative path in bin/gitlab-shell (Fixes: #914496)

 -- Pirate Praveen <praveen@debian.org>  Sun, 02 Dec 2018 12:52:50 +0530

gitlab-shell (8.3.3+dfsg-1) unstable; urgency=medium

  * New upstream version 8.3.3+dfsg
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Nov 2018 11:53:34 +0530

gitlab-shell (8.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version 8.1.1+dfsg

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Nov 2018 18:41:08 +0530

gitlab-shell (7.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version 7.2.0+dfsg

 -- Pirate Praveen <praveen@debian.org>  Fri, 09 Nov 2018 12:12:37 +0530

gitlab-shell (7.1.2+dfsg1-1) unstable; urgency=medium

  * Remove vendored copy of gitaly-proto
  * Use golang-github-sirupsen-logrus-dev from system
  * Use golang-golang-x-crypto-dev from system
  * Use golang-golang-x-sys-dev from system
  * Bump Standards-Version to 4.2.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sun, 21 Oct 2018 11:30:05 +0530

gitlab-shell (7.1.2+dfsg-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Mon, 04 Jun 2018 14:07:49 +0530

gitlab-shell (7.1.2+dfsg-1) experimental; urgency=medium

  * New upstream version 7.1.2+dfsg

 -- Pirate Praveen <praveen@debian.org>  Fri, 11 May 2018 14:29:18 +0530

gitlab-shell (6.0.4+dfsg-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Removed trailing whitespaces from changelog.
  * Update copyright format URL to HTTPS.
  * Vcs-URLs to salsa.debian.org.
  * Build and install golang hooks; arch: all --> any.
  * Build-Depends:
    + dh-golang
    + golang-go
    + golang-golang-x-net-dev
    + golang-golang-x-text-dev
    + golang-gitaly-proto-dev
    + golang-google-grpc-dev
    + golang-google-genproto-dev
    + golang-goprotobuf-dev
    + golang-gopkg-yaml.v2-dev

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Apr 2018 12:37:46 +0530

gitlab-shell (6.0.4-1) unstable; urgency=medium

  * New upstream version 6.0.4

 -- Pirate Praveen <praveen@debian.org>  Fri, 16 Mar 2018 18:08:20 +0530

gitlab-shell (5.8.0-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump standards version to 4.1.3, debhelper compat to 11

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Feb 2018 21:35:29 +0530

gitlab-shell (5.8.0-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Tue, 22 Aug 2017 14:13:20 +0530

gitlab-shell (3.6.6-4) unstable; urgency=medium

  [ Pirate Praveen ]
  * Install config.yml.example in /usr/lib

 -- Balasankar C <balasankarc@autistici.org>  Fri, 24 Feb 2017 16:57:29 +0530

gitlab-shell (3.6.6-3) unstable; urgency=medium

  * Fix typo in git 2.11 support patch (Closes: #854262)

 -- Pirate Praveen <praveen@debian.org>  Tue, 07 Feb 2017 21:06:26 +0530

gitlab-shell (3.6.6-2) unstable; urgency=medium

  * Backport git 2.11 support

 -- Pirate Praveen <praveen@debian.org>  Wed, 18 Jan 2017 13:04:37 +0530

gitlab-shell (3.6.6-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Mon, 31 Oct 2016 15:18:08 +0530

gitlab-shell (3.6.3-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sat, 01 Oct 2016 15:44:40 +0530

gitlab-shell (3.4.0-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches

 -- Pirate Praveen <praveen@debian.org>  Thu, 25 Aug 2016 21:39:53 +0530

gitlab-shell (3.0.0-4) unstable; urgency=medium

  * Move config.yml to /usr/share/doc/gitlab-shell
  * Handle bin directory in install target instead of build

 -- Pirate Praveen <praveen@debian.org>  Thu, 14 Jul 2016 19:24:45 +0530

gitlab-shell (3.0.0-3) unstable; urgency=medium

  * Use tcp connection to redis instead of unix socket (redis default is tcp)

 -- Pirate Praveen <praveen@debian.org>  Wed, 22 Jun 2016 15:27:25 +0530

gitlab-shell (3.0.0-2) unstable; urgency=medium

  * Don't check dependencies in autopkgtest (Closes: #819085)

 -- Pirate Praveen <praveen@debian.org>  Mon, 06 Jun 2016 14:36:01 +0530

gitlab-shell (3.0.0-1) unstable; urgency=medium

  * New upstream release
  * Move log file to /var/log/gitlab-shell and
    move config.yml to /etc/gitlab-shell (Closes: #819374)

 -- Pirate Praveen <praveen@debian.org>  Sat, 04 Jun 2016 21:43:54 +0530

gitlab-shell (2.6.10-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Thu, 21 Jan 2016 22:39:43 +0530

gitlab-shell (2.6.9-1) unstable; urgency=medium

  * New upstream release
  * Fix watch file
  * Fix gitlab-shell paths (update patches)
  * Install hooks directory (Thanks Libor Klepáč)
  * Install VERSION (Thanks Marin Jankovski)

 -- Pirate Praveen <praveen@debian.org>  Fri, 15 Jan 2016 15:26:36 +0530

gitlab-shell (2.6.5-2) unstable; urgency=medium

  * Add patch to use system paths

 -- Pirate Praveen <praveen@debian.org>  Tue, 10 Nov 2015 20:51:33 +0530

gitlab-shell (2.6.5-1) unstable; urgency=medium

  * Initial release (Closes: #804461)

 -- Pirate Praveen <praveen@debian.org>  Mon, 09 Nov 2015 00:33:01 +0530
